// To parse this JSON data, do
//
//     final catApiResponse = catApiResponseFromMap(jsonString);

class CatApiResponse {
  CatApiResponse({
    required this.weight,
    required this.id,
    required this.name,
    required this.image,
    required this.origin,
    required this.description,
    required this.intelligence,
    required this.temperament,
    required this.strangerFriendly,
  });

  String weight;
  String id;
  String name;
  String image;
  String origin;
  String description;
  int intelligence;
  String temperament;
  int strangerFriendly;

  factory CatApiResponse.fromJson(Map<String, dynamic> json) {
    return CatApiResponse(
      id: json['id'],
      name: json['name'],
      weight: json['weight']['metric'],
      image: json['image'] != null
          ? json['image']['url']
          : 'https://w0.peakpx.com/wallpaper/894/391/HD-wallpaper-404-background-apple-designer-error-humor-logo-not-found-silly.jpg',
      origin: json['origin'],
      description: json['description'],
      intelligence: json['intelligence'],
      temperament: json['temperament'],
      strangerFriendly: json['stranger_friendly'],
    );
  }

  static fromListJson(List<dynamic> cats) {
    List<CatApiResponse> models = [];

    for (var cat in cats) {
      models.add(CatApiResponse.fromJson(cat));
    }

    return models;
  }
}
