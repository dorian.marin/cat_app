import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 20,
        shadowColor: Colors.purple,
        backgroundColor: Colors.purple,
        //title: const Text('App Gatitos'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            const Center(
              widthFactor: double.infinity,
              heightFactor: 5,
              child: Text(
                'Catbreeds',
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  color: Colors.purple,
                ),
              ),
            ),
            GestureDetector(
              onTap: () => Navigator.pushNamed(context, 'landing'),
              child: const FadeInImage(
                image: AssetImage('assets/prueba1_cat-removebg.png'),
                placeholder: AssetImage('assets/jar-loading.gif'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
