export 'package:cats_app/screens/alert_screen.dart';
export 'package:cats_app/screens/details_cat_screen.dart';
export 'package:cats_app/screens/details_scroll_screen.dart';
export 'package:cats_app/screens/landing_screen.dart';
export 'package:cats_app/screens/splash_screen.dart';
