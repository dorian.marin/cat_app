import 'package:cats_app/models/cat_api_response.dart';
import 'package:cats_app/providers/cats_providers.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CatSearchDelegate extends SearchDelegate {
  @override
  String get searchFieldLabel => 'Search breed';

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () {
          query = '';
        },
        icon: const Icon(Icons.clear_rounded),
      )
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
      onPressed: () {
        close(context, null);
      },
      icon: const Icon(Icons.arrow_back_ios_new_rounded),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return const Text('buildResults');
  }

  Widget _emptyContainer() {
    return const Center(
      child: Icon(
        Icons.pets_outlined,
        color: Colors.black38,
        size: 140,
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    if (query.isEmpty) {
      return _emptyContainer();
    }

    final catsProvider = Provider.of<CatProvider>(context, listen: false);

    return FutureBuilder(
      future: catsProvider.searchCats(query),
      builder: (_, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) return _emptyContainer();

        final cats = snapshot.data!;

        return ListView.builder(
          itemCount: cats.length,
          itemBuilder: (_, int index) => _CatItem(cats[index]),
        );
      },
    );
  }
}

class _CatItem extends StatelessWidget {
  final CatApiResponse cat;

  const _CatItem(this.cat);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: FadeInImage(
        placeholder: const AssetImage('assets/jar-loading.gif'),
        image: NetworkImage(cat.image),
        width: 50,
        fit: BoxFit.contain,
      ),
    );
  }
}
