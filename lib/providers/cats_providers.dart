// import 'package:cats_app/models/models.dart';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../models/cat_api_response.dart';

class CatProvider extends ChangeNotifier {
  final String _apiKey = 'bda53789-d59e-46cd-9bc4-2936630fde39';
  final String _baseUrl = 'api.thecatapi.com';

  List<CatApiResponse> breedsCats = [];

  // List<Cat> onDisplayCats = [];

  CatProvider() {
    getOnDisplayedCats();
  }

  getOnDisplayedCats() async {
    var url = Uri.https(_baseUrl, 'v1/breeds', {
      'api_key': _apiKey,
    });

    final response = await http.get(url);
    final decodedData = json.decode(response.body);
    final List<CatApiResponse> catApiResponse =
        CatApiResponse.fromListJson(decodedData);

    // print(catApiResponse.name[0]);
    // print(catApiResponse[0].image);

    breedsCats = catApiResponse;

    notifyListeners();
  }

  Future<CatApiResponse> searchCats(String query) async {
    // final String _apiKey = 'bda53789-d59e-46cd-9bc4-2936630fde39';
    // ignore: no_leading_underscores_for_local_identifiers
    const String _baseUrl = 'api.thecatapi.com';

    final url = Uri.https(_baseUrl, 'v1/breeds/search', {'query': query});

    final response = await http.get(url);
    final decodedData = json.decode(response.body);
    /* final List<CatApiResponse> catApiResponse =
        CatApiResponse.fromListJson(decodedData); */

    final searchResponse = CatApiResponse.fromListJson(decodedData);
    /* final decodedData = json.decode(response.body);

    
    final List<CatApiResponse> searchResponse =
        CatApiResponse.fromListJson(decodedData);*/

    return searchResponse.results;
  }
}
