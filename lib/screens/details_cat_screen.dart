import 'package:flutter/material.dart';

import '../models/models.dart';

class DetailsScreen extends StatelessWidget {
  const DetailsScreen({Key? key}) : super(key: key);

  // final List<CatApiResponse> cats;

  @override
  Widget build(BuildContext context) {
    final CatApiResponse cat =
        ModalRoute.of(context)!.settings.arguments as CatApiResponse;

    // print(cat.name);
    // print(cats[1].id);

    return Scaffold(
      // _CustomAppBar(cat),
      appBar: AppBar(
        title: Text(cat.name),
        shadowColor: Colors.purple,
        backgroundColor: Colors.purple,
        titleTextStyle: const TextStyle(
          color: Colors.white70,
          fontSize: 24,
        ),
      ),
      /* body: CustomScrollView(
        slivers: [
          SliverList(
              delegate: SliverChildDelegate([
                _OverView(),
                
                // _CatDestails(cat),
          ]))
        ],
      ), */

      body: ListView.builder(
          itemCount: 1,
          itemBuilder: (_, __) {
            return Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: [
                  // CustomScrollView(),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: FadeInImage(
                      height: 320,
                      width: double.infinity,
                      placeholder: const NetworkImage(
                          'https://i0.wp.com/icon-library.com/images/loading-gif-icon/loading-gif-icon-9.jpg?resize=650,400'),
                      image: NetworkImage(cat.image),
                      fit: BoxFit.cover,
                    ),
                  ),
                  const Text(
                    'Breed Description',
                    style: TextStyle(
                      fontSize: 20,
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                      child: Text(
                        cat.description,
                        style: const TextStyle(fontSize: 16),
                      )),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const _TextDefault('ID : '),
                      Text(cat.id),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const _TextDefault('Country Origin: '),
                      Text(cat.origin),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const _TextDefault('Metric Weight: '),
                      Text(cat.weight),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const _TextDefault('Intelligence: '),
                      Text(cat.intelligence.toString()),
                    ],
                  ),
                  /* Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const _TextDefault('temperament: '),
                    Text(
                      cat.temperament,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                  ],
                ), */
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const _TextDefault('Stranger Friendly: '),
                      Text(cat.strangerFriendly.toString()),
                    ],
                  ),
                  Padding(
                      padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                      child: Text(
                        cat.description,
                        style: const TextStyle(fontSize: 16),
                      )),
                ],
              ),
            );
          }),
    );
  }
}

class _TextDefault extends StatelessWidget {
  final String texto;
  const _TextDefault(this.texto);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0, top: 8.0),
      child: Text(
        texto,
        style: const TextStyle(
          fontSize: 16,
          fontStyle: FontStyle.italic,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
