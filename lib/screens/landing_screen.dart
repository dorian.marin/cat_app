import 'package:flutter/material.dart';

import 'package:cats_app/providers/cats_providers.dart';
import 'package:cats_app/search/search_delegate.dart';

import 'package:cats_app/widgets/widgets.dart';
import 'package:provider/provider.dart';

class LandingScreen extends StatelessWidget {
  const LandingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final catsProvider = Provider.of<CatProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Catbreeds'),
        shadowColor: Colors.purple,
        backgroundColor: Colors.purple,
        titleTextStyle: const TextStyle(
          color: Colors.white70,
          fontSize: 24,
        ),
        actions: [
          IconButton(
            onPressed: () =>
                showSearch(context: context, delegate: CatSearchDelegate()),
            icon: const Icon(Icons.search_rounded),
          )
        ],
      ),
      body: CardBreeds(cats: catsProvider.breedsCats),
    );
  }
}
/*
class _CustomAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      backgroundColor: Colors.deepPurple,
      expandedHeight: 200, //Aumenta el alto del AppBar
      // floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
          centerTitle: true,
          titlePadding: const EdgeInsets.all(
              0), //Elimina el padding entre el texto y el contenedor
          title: Container(
              width: double.infinity,
              alignment: Alignment.bottomCenter,
              color: Colors.black12,
              //Permite que tenga un buen color al fondo blanco de la letra y que no se pierda sin importar la imagen que tenga de fondo.
              padding: const EdgeInsets.only(bottom: 15),
              child: const Text(
                'Razas de gatos',
                style: TextStyle(
                  fontSize: 16,
                ),
              )),
          background: const FadeInImage(
            placeholder: AssetImage('assets/jar-loading.gif'),
            image: NetworkImage(
                'https://c.tenor.com/DZKcXvpu-d8AAAAC/bongo-cat-cute-png.gif'),
            // image: NetworkImage('https://www.via.placeholder.com/500x300'),
            fit: BoxFit.cover,
          )),
    );
  }
}
*/
/*
class _SearchBreeds extends StatelessWidget {
  const _SearchBreeds({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 50,
      color: Colors.red,
      child: Row(
        children: [
          const Text(
            'Parte de busqueda',
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
          GestureDetector(
            child: const Icon(
              Icons.search_outlined,
            ),
            onTap: () {
              print('test!!1');
            },
          ),
        ],
      ),
    );
  }
}
*/