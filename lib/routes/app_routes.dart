import 'package:flutter/material.dart';

import 'package:cats_app/screens/screens.dart';
import '../models/menu_option.dart';

class AppRoutes {
  static const initialRoute = 'splash';

  static final menuOptions = <MenuOption>[
    MenuOption(
        route: 'splash', name: 'Spash Screen', screen: const SplashScreen()),
    MenuOption(
        route: 'landing',
        name: 'Landing Screen',
        screen: const LandingScreen()),
    MenuOption(
        route: 'details',
        name: 'Details Screen',
        screen: const DetailsScreen()),
  ];

  static Map<String, Widget Function(BuildContext)> getAppRoutes() {
    Map<String, Widget Function(BuildContext)> appRoutes = {};

    for (final option in menuOptions) {
      appRoutes.addAll({option.route: (BuildContext context) => option.screen});
    }

    return appRoutes;
  }

  static Route<dynamic> onGeneratedRoute(RouteSettings settings) {
    //De esta forma podemos ir a una ruta default cuando se redirecciona a una ruta no existente por ejemplo.
    return MaterialPageRoute(
      builder: (context) => const AlertScreen(),
    );
  }
}
