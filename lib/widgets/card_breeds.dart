import 'package:flutter/material.dart';

import '../models/models.dart';

class CardBreeds extends StatelessWidget {
  final List<CatApiResponse> cats;

  const CardBreeds({super.key, required this.cats});

  @override
  Widget build(BuildContext context) {
    // final catsProvider = Provider.of<CatProvider>(context);
    // print(cats.length);

    return ListView.separated(
      itemCount: cats.length,
      itemBuilder: (context, index) {
        final cat = cats[index];

        // print(cats.length);
        // print(cat.image[index]);

        return Container(
          width: double.infinity,
          height: 420,
          // color: Colors.green,
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    cat.name,
                    style: const TextStyle(
                      fontSize: 16,
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.w500,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text(
                    'Metric weight: ${cat.weight}',
                    style: const TextStyle(
                      fontSize: 16,
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
              GestureDetector(
                onTap: () =>
                    Navigator.pushNamed(context, 'details', arguments: cat),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(30),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0),
                    child: FadeInImage(
                      height: 320,
                      width: double.infinity,
                      placeholder: const NetworkImage(
                          'https://i0.wp.com/icon-library.com/images/loading-gif-icon/loading-gif-icon-9.jpg?resize=650,400'),
                      image: NetworkImage(cat.image),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    cat.origin,
                    style: const TextStyle(
                        fontSize: 16,
                        fontStyle: FontStyle.italic,
                        fontWeight: FontWeight.w500),
                  ),
                  Text(
                    'Intelligence: ${cat.intelligence.toString()}',
                    style: const TextStyle(
                        fontSize: 16,
                        fontStyle: FontStyle.italic,
                        fontWeight: FontWeight.w500),
                  ),
                ],
              ),
            ],
          ),
        );
      },
      separatorBuilder: (_, __) => const Divider(),
    );
  }
}
